# Copyright 2019 Paul Wise
# Released under the MIT/Expat license, see doc/COPYING

from os import stat, umask, fchmod
from atomicwrites import AtomicWriter, atomic_write

# FIXME: drop permissions workaround when no longer needed
# https://github.com/untitaker/python-atomicwrites/issues/42
class AtomicWriterPerms(AtomicWriter):
	def get_fileobject(self, **kwargs):
		f = super().get_fileobject(**kwargs)
		try:
			mode = stat(self._path).st_mode
		except FileNotFoundError:
			# Creating a new file, emulate what os.open() does
			mask = umask(0)
			umask(mask)
			mode = 0o777 & ~mask
		fd = f.fileno()
		fchmod(fd, mode)
		return f

# FIXME: drop prefix workaround when no longer needed
# https://github.com/untitaker/python-atomicwrites/issues/37
# https://github.com/untitaker/python-atomicwrites/issues/38
# https://github.com/untitaker/python-atomicwrites/issues/39
# https://github.com/untitaker/python-atomicwrites/issues/40
class AtomicWriterPrefix(AtomicWriterPerms):
	def __init__(self, path, prefix=None, **kwargs):
		self._prefix = prefix
		kwargs.pop('prefix', None)
		super().__init__(path, **kwargs)

	def get_fileobject(self, **kwargs):
		kwargs.pop('prefix', None)
		return super().get_fileobject(prefix=self._prefix, **kwargs)

def overwrite(path, **kwargs):
	try:
		return atomic_write(path, writer_cls=AtomicWriterPerms, overwrite=True, prefix=path+'.tmp.', **kwargs)
	except TypeError:
		return atomic_write(path, writer_cls=AtomicWriterPrefix, overwrite=True, prefix=path+'.tmp.', **kwargs)
