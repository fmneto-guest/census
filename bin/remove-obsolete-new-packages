#!/usr/bin/python3

# Copyright 2019 Anastasia Tsikoza
# Released under the MIT/Expat license, see doc/COPYING

# Removes obsolete source packages which are not derived from Debian
#
# Usage:
# remove-obsolete-new-packages <obsolete packages> <source files list> <log file>

# FIXME: add more logging, especially for time consuming loading/saving operations

from datetime import datetime
import logging
import os
import sys
import yaml
from census import overwrite

sources_new_obsolete = sys.argv[1]
sources_files = sys.argv[2]
log_file = sys.argv[3]
sha1_cache_dir = 'sha1-farm'
md5_cache_dir = 'md5-farm'
sha256_cache_dir = 'sha256-farm'
sha1_changelog_dir = 'sha1-changelog'

hash_types = {
    32 : 'md5sum',
    40 : 'sha1',
    64 : 'sha256',
}


def hash_path(dir, hash, description):
    """Returns the path to the file in the hash-based filesystem.

    Args:
        dir (str): A name of the cache directory.
        hash (str): A name of the file in the cache directory.
        description (str): The file's description to be written into the log.

    Returns:
        str: A path to the file in the hash-based filesystem.

    Raises:
        TypeError: An error occurred if the hash is None.
    """
    try:
        path = os.path.join(dir, hash[0:2], hash[2:4], hash)
        if os.path.exists(path):
            logging.debug('%s found: %s', description, path)
            return path
        else:
            logging.debug('%s not found', description)
            return None
    except TypeError:
        return None

def get_basename(path):
    """Returns the filename extracted from the path."""
    try:
        return os.path.basename(path)
    except AttributeError:
        return None


def rel_target_path(link_path):
    """Finds the path to which the symlink points.

    Args:
        link_path (str): A path to the symlink to be processed.

    Returns:
        str: A relative path to the symlink's target.

    Raises:
        AttributeError: An error occurred if the link_path is None.
        FileNotFoundError: An error occurred accessing a non-existent file.
        OSError: An error occurred accessing a file which is not a symlink.
    """
    try:
        target_path = os.path.join(os.path.dirname(link_path),
                                   os.readlink(link_path))
        rel_target_path = os.path.relpath(target_path)
        logging.debug('symlink to sha1 cache found: %s, %s',
                       link_path, rel_target_path)
        if not os.path.exists(rel_target_path):
            logging.warning("broken symlink, target doesn't exist")
            return None
        return rel_target_path
    except AttributeError:
        return None
    except FileNotFoundError:
        logging.warning('symlink to sha1 cache not found')
        return None
    except OSError:
        logging.warning('not a symlink: %s', link_path)
        return None


def remove(path, description=None):
    """Removes the file and the containing directory if it becomes empty.

    Args:
        path (str): A path to the file to be removed.
        description (str): The file's description to be written into the log
            (default None).

    Raises:
        OSError: An error occurred accessing the non-existent file.
        TypeError: An error occurred if the path is None.
    """
    try:
        os.remove(path)
        if description and path:
            logging.debug('removing %s %s', description, path)
    except (OSError, TypeError):
        pass
    try:
        os.rmdir(os.path.dirname(path))
    except (OSError, TypeError):
        pass

# Load data
try:
    with open(sources_files) as f:
        files = yaml.load(f, Loader=yaml.CSafeLoader)
except IOError:
    files = {}

try:
    with open(sources_new_obsolete) as f:
        data = yaml.load(f, Loader=yaml.CSafeLoader)
except IOError:
    data = {}

# Configure logging
if data:
    try:
        os.remove(log_file)
    except OSError:
        pass
    logging.basicConfig(format='%(levelname)s: %(message)s',
                        level=logging.DEBUG, filename=log_file)
    today = datetime.now().strftime('%b %d %Y')
    logging.info('date: %s', today)

# Parse the data and remove all the obsolete files
for name, versions in data.items():
    for version, hashes in versions.items():
        logging.debug('started processing source package %s %s', name, version)
        logging.debug('found %s files to be removed', str(len(hashes)))
        for hash in hashes:
            hash_type = hash_types.get(len(hash), 'unknown')
            logging.debug('processing %s hash: %s', hash_type, hash)
            hash_cache_path, sha1_cache_path, changelog_path = None, None, None
            if hash_type == 'sha1':
                sha1_cache_path = hash_path(sha1_cache_dir, hash,
                                            'file in sha1 cache')
                changelog_path = hash_path(sha1_changelog_dir, hash,
                                           'changelog file')
            else:
                if hash_type == 'sha256':
                    cache_dir = sha256_cache_dir
                elif hash_type == 'md5sum':
                    cache_dir = md5_cache_dir
                else:
                    logging.warning('unknown hash type detected: %s', hash)
                    continue
                hash_cache_path = hash_path(cache_dir, hash,
                                            'file in %s cache' % hash_type)
                sha1_cache_path = rel_target_path(hash_cache_path)
                changelog_path = hash_path(sha1_changelog_dir,
                                           get_basename(sha1_cache_path),
                                           'changelog file')
            if sha1_cache_path:
                remove(sha1_cache_path, 'file from sha1 cache')
            if hash_cache_path:
                remove(hash_cache_path, 'symlink from %s cache' % hash_type)
            if changelog_path:
                remove(changelog_path, 'changelog file')
            try:
                logging.debug('removing entry from sources.files: %s', files[hash])
                del files[hash]
            except KeyError:
                pass

        logging.debug('finished processing source package %s %s', name, version)
        logging.debug('')

logging.shutdown()

with overwrite(sources_files) as f:
   yaml.dump(files, f, Dumper=yaml.CSafeDumper)
