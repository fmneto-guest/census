#!/usr/bin/python3

# Copyright 2012 Paul Wise
# Released under the MIT/Expat license, see doc/COPYING

# Aggregates the list of changed source packages
#
# Usage:
# aggregate-sources-patches <list of derivatives> <sources patches list>

import os
import sys
import yaml
from datetime import date

def shortslug(name):
	return name[:4] if name.startswith('lib') else name[0]

url = 'http://deriv.debian.net/patches'
derivatives = sys.argv[1:-2]
sources_patches = sys.argv[-2]
sources_patches_pts = sys.argv[-1]


# Load the old information about patched versions
try:
	f = open(sources_patches)
	old_patches = yaml.load(f, Loader=yaml.CSafeLoader)
	f.close()
except IOError:
	old_patches = []


# Load new data about patches from individual derivatives
new_patches = []
for deriv in derivatives:
	# We explicitly do not ignore inactive derivatives since
	# their files may go missing at any time.
	try:
		f = open(os.path.join(deriv,'sources.patches'))
		data = yaml.load(f, Loader=yaml.CSafeLoader)
		f.close()
	except IOError:
		continue
	for patch in data:
		patch['deriv'] = deriv
		new_patches.append(patch)


# Merge the old patches with the new patches
for patch in old_patches:
	obsolete = patch.pop('obsolete', False)
	try: new_patches.remove(patch)
	except ValueError: obsolete = True
	# If the patch is not present in the new patches list, it is considered
	# obsolete, otherwise its last seen field is updated
	if obsolete: patch['obsolete'] = obsolete
	else:
		patch['last seen'] = date.today().strftime('%Y-%m-%d')

patches = old_patches + new_patches


# Calculate the output

# List of package names and URLs for the PTS
# Need to exclude Ubuntu since the PTS has other links to its patches
debian_names = set([patch['debian_name'] for patch in patches if not patch.get('obsolete', False) and patch['deriv'] != 'Ubuntu'])
patches_pts = dict((name, '/'.join((url,shortslug(name),name,''))) for name in debian_names)
with overwrite(sources_patches_pts) as f:
	yaml.dump(patches_pts, f, Dumper=yaml.CSafeDumper)

# The general patch metadata
with overwrite(sources_patches) as f:
	yaml.dump(patches, f, Dumper=yaml.CSafeDumper)
